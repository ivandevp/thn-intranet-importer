class Customer
	def initialize(first_name, last_name, dni, email, ruc, customer_no)
		@firstName = first_name
		@lastName = last_name
		@dni = dni
		@email = email
		@ruc = ruc
		@customerNo = customer_no
	end
end