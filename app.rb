# Main file

require 'spreadsheet'
require 'net/http'
require 'uri'
require 'json'

load 'models/Customer.rb'

workbook = Spreadsheet.open 'catalogos.xls'
worksheet = workbook.worksheet 0

customers = []

File.open('customers.txt', 'w') do |file| 
	worksheet.each do |row|
		next if row[0].nil?
		nombre = row[4].to_s
		apellidos = row[2].to_s + " " + row[3].to_s
		dni = row[6].to_i.to_s
		ruc = ''
		email = row[8].to_s
		codigo = row[1].to_i.to_s
		if row[4].nil?
			nombre = row[5].to_s
		end
		if row[8].nil?
			email = dni.to_s + "@catalogothn.com.pe"
		end
		if dni.length > 8
			ruc = dni
		end
		customer = "Nombre: " + nombre + ", Apellido: " + apellidos +
			", DNI: " + dni + ", Email: " + email + ", RUC: " +
			ruc + ", Customer No: " + codigo
		
		print customer

		customers << {
			firstName: nombre,
			lastName: apellidos,
			email: email,
			dni: dni,
			ruc: ruc,
			customerNo: codigo
		}

		file.write(customer + "\n")
	end
end

uri = URI.parse('http://intranet.catalogothn.com.pe/import/massive')
# header = { 'Content-Type': 'text/json' }
data = {
	users: {
		customers: customers,
		employees: []
	}
}

# Create the HTTP objects
http = Net::HTTP.new(uri.host, uri.port)
http.read_timeout = 21600
request = Net::HTTP::Post.new(uri.request_uri)
request.body = data.to_json

# Send the request
response = http.request(request)

File.open('log.html', 'w') do |file|
	file.write(response.body)
end
